import React, {useState} from 'react';
import FirstForm from "../../Container/FirstForm/FirstForm";
import SecondForm from "../../Container/SecondForm/SecondForm";
import Choice from "../../Container/Choice/Choice";
import {nanoid} from 'nanoid';

const sum = people => {
    let sum = 0;
    people.map(person => {
        sum += person.price
    })
    return sum
}

const Expense = () => {
    const [mode, setMode] = useState('split');
    const [people, setPeople] = useState([]);
    const [tips, setTips] = useState(0);
    const [delivery, setDelivery] = useState(0);
    const [total, setTotal] = useState(0);
    const [count, setCount] = useState(0);
    const [priceOrder, setPriceOrder] = useState(0);
    const [onePerson, setOnePerson] = useState(0);

    const onRadioChange = e => {
        setMode(e.target.value);
    };
    const addPerson = () => {
        setPeople(people => [...people, {name: '', price: '', id: nanoid()}]);
    };

    const changePersonField = (id, name, value) => {
        setPeople(people => {
            return people.map(person => {
                if (person.id === id){
                    return {...person, [name]: value}
                }
                return person;
            });
        })
    };
    const changePersonSum = (id, price, value) => {
        if(!parseInt(value)){
            alert('Please enter number');
            return;
        }
        setPeople(people => {
            return people.map(person => {
                if (person.id === id){
                    return {...person, [price]: parseInt(value)}
                }
                return person;
            });
        });
        sum(people);
    };

    const changeTips = value => {
        if(!parseInt(value)){
            alert('Please enter number');
            return;
        }
        setTips(parseInt(value))
    }

    const changeDelivery = value => {
        if(!parseInt(value)){
            alert('Please enter number');
            return;
        }
        setDelivery(parseInt(value))
    }
    const changeCount = value => {
        if(!parseInt(value)){
            alert('Please enter number');
            return;
        }
        setCount(parseInt(value))
    }
    const changePriceOfOrder = value => {
        if(!parseInt(value)){
            alert('Please enter number');
            return;
        }
        setPriceOrder(parseInt(value))
    }

    const calculateEqually = () => {
        const priceEqually = priceOrder + (priceOrder * tips / 100) + delivery;
        setOnePerson(Math.ceil(priceEqually / count));
    }

    const calculate = () => {
        const price = Math.ceil((sum(people)) + (((tips * sum(people)) / 100) + delivery));
        setPeople(people => people.map(person => {
            return {...person, price: person.price + Math.ceil((person.price * tips / 100) + (delivery / people.length))}
        }))
        setTotal(price);
    }
    const remove = id => {
        setPeople(people.filter(p => p.id !== id));
    };
    return (
        <div className="container">
           <Choice
           mode={mode}
           onRadioChange={onRadioChange}
           />
            <div>
                {mode === 'split' ? (
                    <FirstForm
                        changeCount={changeCount}
                        changePriceOfOrder={changePriceOfOrder}
                        changeTips={changeTips}
                        changeDelivery={changeDelivery}
                        calculateEqually={calculateEqually}
                        onePerson={onePerson}
                        priceOrder={priceOrder + (priceOrder * tips / 100) + delivery}
                        count={count}
                    />
                ) : (
                    <SecondForm
                    people={people}
                    changePersonField={changePersonField}
                    changePersonSum={changePersonSum}
                    addPerson={addPerson}
                    changeTips={changeTips}
                    changeDelivery={changeDelivery}
                    calculate={calculate}
                    total={total}
                    remove={remove}
                    />
                )}
            </div>
        </div>
    );
};

export default Expense;