import React from 'react';
import './Choice.css'

const Choice = props => (
    <>
        <div className="form-choice">
            <label>
                <input
                    type="radio"
                    checked={props.mode === 'split'}
                    name="mode"
                    value="split"
                    onChange={props.onRadioChange}
                />{' '}
                Equally between the participants
            </label>
        </div>
        <div className="form-choice">
            <label>
                <input
                    type="radio"
                    name="mode"
                    value="individual"
                    checked={props.mode === 'individual'}
                    onChange={props.onRadioChange}
                />{' '}
                For each individual
            </label>
        </div>
    </>
)

export default Choice;