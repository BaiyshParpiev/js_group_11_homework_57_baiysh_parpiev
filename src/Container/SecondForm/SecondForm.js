import React from 'react';
import './secondForm.css';

const SecondForm = props => (
    <form>
        <div className="secondForm">
            {props.people.map(person => (
                <div key={person.id}>
                    <input type="text"
                           placeholder="Name"
                           onChange={(e) =>
                               props.changePersonField(person.id, 'name', e.target.value)}/>
                    <input type="number"
                           placeholder="Sum"
                           onChange={(e) =>
                               props.changePersonSum(person.id, 'price', e.target.value)}/>
                    <button type="button" onClick={() => props.remove(person.id)}>Remove</button>
                </div>
            ))}
            <button type="button" onClick={props.addPerson}>Add person</button>
            <div>
                <label>Tips
                    <input type="text"
                           placeholder='Percent tips %'
                           onChange={e => props.changeTips(e.target.value)}
                    /> %
                </label>
            </div>
            <div>
                <label>Delivery
                    <input type="text"
                           placeholder='Delivery'
                           onChange={e => props.changeDelivery(e.target.value)}
                    />
                </label>
            </div>
            <button type='button' onClick={props.calculate}>Calculate</button>
            <p>Total Price : {props.total}</p>
            <p>{props.people.map(person => (<span key={person.id}>{person.name} : {person.price}som </span>))}</p>
        </div>
    </form>
)

export default SecondForm;