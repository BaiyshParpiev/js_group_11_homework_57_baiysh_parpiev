import React from 'react';
import './FirstForm.css'

const FirstForm = props => (
        <form>
           <div className="firstForm">
               <div className="forms">
                   <label>Count of People
                       <input type="text"
                              placeholder='Count of People'
                              onChange={e => props.changeCount(e.target.value)}
                       />
                   </label>
               </div>
               <div className="forms">
                   <label> Price of order
                       <input type="text"
                              placeholder='Total sum'
                              onChange={e => props.changePriceOfOrder(e.target.value)}
                       />
                   </label>
               </div>
               <div className="forms">
                   <label>Tips
                       <input type="text"
                              placeholder='Percent tips %'
                              onChange={e => props.changeTips(e.target.value)}
                       /> %
                   </label>
               </div>
               <div className="forms">
                   <label>Delivery
                       <input type="text"
                              placeholder='Delivery'
                              onChange={e => props.changeDelivery(e.target.value)}
                       />
                   </label>
               </div>
               <button type='button' onClick={props.calculateEqually}>Calculate</button>
               <p>Total Price: {props.priceOrder} <br/>Equally  : {props.onePerson} <br/> Count of People: {props.count} <br/></p>
           </div>
        </form>
);

export default FirstForm;